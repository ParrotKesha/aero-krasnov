<?php
class User
{
    private $fullName;
    private $phoneNumber;
    private $birthday;
    private $email;
    private $comment;
    public $errors;

    public function __construct($fullName, $phoneNumber, $birthday, $email, $comment)
    {
        $this->fullName = $fullName;
        $this->phoneNumber = $phoneNumber;
        $this->birthday = $birthday;
        $this->email = $email;
        $this->comment = $comment;
        $this->errors = array();
        $this->checkUser();
    }

    private function checkUser()
    {
        $this->checkFields();
        if (count($this->errors) != 1) {
            $this->checkFullName();
            $this->checkPhoneNumber();
            $this->checkEmail();
        }
    }

    private function checkFields()
    {
        if ($this->fullName == "" || $this->phoneNumber == "" || $this->birthday == "" || $this->email == "" || $this->comment == "") {
            array_push($this->errors, "Заполните все поля!");
        }
    }

    private function checkFullName()
    {
        $name = explode(" ", $this->fullName);
        if (count($name) != 3) {
            array_push($this->errors, "Введите Фамилию, Имя и Отчество!");
        } else {
            if (!preg_match('/[а-яё]/iu', $name[0]) || !preg_match('/[а-яё]/iu', $name[1]) || !preg_match('/[а-яё]/iu', $name[2])) {
                array_push($this->errors, "ФИО может содержать только кириллицу!");
            }
        }
    }

    private function checkPhoneNumber()
    {
        if (!preg_match('/^(8|\+7)\d{10}$/', $this->phoneNumber)) {
            array_push($this->errors, "Неверный формат телефона!");
        }
    }

    private function checkEmail()
    {
        if (!preg_match('/^[a-zA-Z0-9_\-.]+@[a-zA-Z0-9\-]+\.[a-zA-Z]+$/', $this->email)) {
            array_push($this->errors, "Неверный формат E-mail!");
        }
    }

    public function getFullName()
    {
        return $this->fullName;
    }

    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    public function getBirthday()
    {
        return $this->birthday;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getComment()
    {
        return $this->comment;
    }
}
