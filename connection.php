<?php

require_once 'user.php';

class Connection
{
    private $host;
    private $database;
    private $user;
    private $password;

    public function __construct($host, $database, $user, $password)
    {
        $this->host = $host;
        $this->database = $database;
        $this->user = $user;
        $this->password = $password;
    }

    public function writeToDatabase($user)
    {
        $db = mysqli_connect($this->host, $this->user, $this->password, $this->database) or die(mysqli_error($db));
        mysqli_query($db, "SET NAMES 'utf8mb4'");

        $fullname = mysqli_real_escape_string($db, $user->getFullName());
        $phonenumber = mysqli_real_escape_string($db, $user->getPhoneNumber());
        $birthday = mysqli_real_escape_string($db, $user->getBirthday());
        $email = mysqli_real_escape_string($db, $user->getEmail());
        $comment = mysqli_real_escape_string($db, $user->getComment());

        $query = 'INSERT INTO academy (fullname, phonenumber, email, birthday, comment) VALUES (?, ?, ?, ?, ?);';
        $statement = $db->prepare($query);
        $statement->bind_param("sssss", $fullname, $phonenumber, $email, $birthday, $comment);
        $statement->execute();

        mysqli_close($db);
    }

    public function getHost()
    {
        return $$this->host;
    }

    public function getDatabase()
    {
        return $this->database;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getPassword()
    {
        return $this->password;
    }
}
