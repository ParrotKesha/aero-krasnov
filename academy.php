<?php
session_start();
require_once 'connection.php';
require_once 'captcha.php';
require_once 'user.php';

if (isset($_POST['submit'])) {
    $captcha = new Captcha();

    if ($captcha->getResponse()->success) {
        $user = new User(
            htmlspecialchars($_POST["full_name"]),
            htmlspecialchars($_POST["phone_number"]),
            htmlspecialchars($_POST["birthday"]),
            htmlspecialchars($_POST["email"]),
            htmlspecialchars($_POST["comment"])
        );

        if (count($user->errors) == 0) {
            $connection = new Connection('localhost', 'aero', 'root', 'qwerty');
            $connection->writeToDatabase($user);
            $_SESSION['complete'] = true;
        } else {
            $_SESSION['errors'] = $user->errors;
            $_SESSION['complete'] = false;
        }
    } else {
        $_SESSION['captchaError'] = true;
    }
}
header('Location: /index.php');
