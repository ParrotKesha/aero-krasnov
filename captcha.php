<?php

class Captcha
{
    const SECRET_KEY = "6LdunJoUAAAAAFN5ULh1o05oLlzoGj2REhKjH1lO";
    private $responseKey;
    private $userIP;
    private $url;
    private $response;

    public function __construct()
    {
        $this->responseKey = $_POST['g-recaptcha-response'];
        $this->userIP = $_SERVER['REMOTE_ADDR'];
        $this->url = "https://www.google.com/recaptcha/api/siteverify?secret=".self::SECRET_KEY."&response=$this->responseKey&remoteip=$this->userIP";
        $this->response = file_get_contents($this->url);
        $this->response = json_decode($this->response);
    }

    public function getResponse()
    {
        return $this->response;
    }
}
